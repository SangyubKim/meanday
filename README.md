# README #

[ MeanDay ] Class

평균이동선을 그리기 위한 class

생성자 : 종목/코스피,코스닥/년/월/일 데이터 입력하면 해당 파라미터에 맞는 주식 데이터 산출

getRealData : 생성자에서 생선한 데이터의 일별 종가

makeMeanData : standard 파라미터에 맞는 평균이동선 데이터 산출

makeGrape : 전달된 List Parameter별 그래프 그려줌.


[ 삼성전자 6개월 데이터 20년 8월 6일 기준]

![Samsung6Month](./image/Samsung6Month.png)