import pandas_datareader as pdr
from pandas import DataFrame
import datetime
import matplotlib.pyplot as plt
import time

class MeanDay:
    # 종목 / 코스닥,코스피( KS or KQ ) / 몇년치 / 몇개월치 / 몇일치  / sqlData / sql사용 여부
    def __init__(self, jongmok=None, type=None, howManyYear=None, howManyMonth=None, howMany=None, data=None,sqlData=False):
        if sqlData != False:
            self.data = data
            # 종가 데이터
            self.list = []
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 500초 평균선
            first = self.makeMeanData(1500)
            self.list.append([first, 'g'])

            # 5000초 평균선
            sec = self.makeMeanData(5000)
            self.list.append([sec, 'b'])

            # 20일 평균선
            # th = self.makeMeanData(20)
            # list.append([th, 'y'])

            #self.makeGrape(self.list)

        else:
            tm = datetime.datetime.now()
            startTm = str(tm.year - howManyYear) + "-" + str(tm.month - howManyMonth) + "-" + str(tm.day - howMany)     ### 수정 필요
            endTm = str(tm.year) + "-" + str(tm.month) + "-" + str(tm.day) ### 수정 필요

            # startTm 부터 endTm까지의 주식 데이터 가져오기
            self.data = pdr.get_data_yahoo(jongmok + '.' + type, start=startTm, end=endTm)

            '''
            ## 현재가 추가를 위한 방법
            new_raw = DataFrame([(0,0,0,0,0,50000)],columns=self.data.columns,index=[datetime.datetime.now()])
            self.data = self.data.append(new_raw)
            '''
            print(self.data)

            self.list = []

            # 종가 데이터
            real = self.getRealData()
            self.list.append([real, '^r--'])

            # 5일 평균선
            first = self.makeMeanData(5)
            self.list.append([first, 'g'])

            # 10일 평균선
            sec = self.makeMeanData(10)
            self.list.append([sec, 'b'])

            # 20일 평균선
            th = self.makeMeanData(20)
            self.list.append([th, 'y'])

            #self.makeGrape(self.list)


    ## 종가 데이터를 가져오기 위한 함수
    def getRealData(self):
        # 종가 데이터
        ret = self.data['Adj Close']
        ret.__setattr__("name","real")      ## name 추가
        return ret

    ##  standard(일) 평균 선을 만들어주는 함수
    def makeMeanData(self,standard):
        # standard (5일,10일,20일 etc...)에 맞게 rolling 후 평균 산출
        mean =self.data['Adj Close'].rolling(window=standard).mean()
        mean.__setattr__("name",str(standard))      ## name 추가
        return mean

    ## listDict[일자] = list[종가,5일평균선,10일 평균선 , 20일 평균선] 형식의 데이터 Return
    def returnAllData(self):
        listDict={}
        for i in self.list:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j]=[i[0][j],]

        return listDict

    ## list = [data , 그래프 색상]
    def makeGrape(self,list):
        ## listDict[일자] = list[종가,5일평균선,10일 평균선 , 20일 평균선] 형식의 데이터 Return
        listDict={}
        for i in list:
            for j in i[0].keys():
                if listDict.__contains__(j):
                    listDict[j].append(i[0][j])
                else:
                    listDict[j]=[i[0][j],]

        ## 그래프 그리기 시작
        plt.figure()
        # 종가,5평선,10평선,20평선 별 그래프 그리기
        for item in list:       # list : [종가,5일평균선,10일평균선,20일 평균선]
            eachList =[]
            keyList=[]
            # value값 분류
            for each in item[0]:
                eachList.append(each)
            # list값 분류
            for key in item[0].index:
                keyList.append(key)

            plt.plot(keyList, eachList, item[1], label=item[0].__getattribute__("name"))


        key = sorted(listDict)
        print(listDict)
        sellList = []
        sellList.append(None)
        pre = key.pop(0)
        # 매수 조건
        for i in key:
            ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클시
            if self.isRealRightUp(listDict[pre],listDict[i]) and self.is5DayMeanRightUp(listDict[pre],listDict[i]) and self.isUP5to10(listDict[i]):
            ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클 시 and 하루에 1000원 이상 올랐을 시
            #if (self.isRealRightUp(listDict[pre], listDict[i]) and self.is5DayMeanRightUp(listDict[pre], listDict[i]) and self.isUP5to10(listDict[i])) or self.isIncrease(listDict[pre], listDict[i]):
                sellList.append(listDict[i][0])
            else:
                sellList.append(None)
            pre = i
        plt.plot(keyList, sellList, 'o', label="sell")

        # 매도 조건
        key = sorted(listDict)
        buyList = []
        buyList.append(None)
        pre = key.pop(0)
        for i in key:
            ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시
            #if self.isRightDown(listDict[pre],listDict[i]) and self.is5DayMeanRightDown(listDict[pre],listDict[i]) and self.isDown5to10(listDict[i]):
            ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시 and 하루에 1000원 이상 떨어졌을 시
            if (self.isRightDown(listDict[pre], listDict[i]) and self.is5DayMeanRightDown(listDict[pre], listDict[i]) and self.isDown5to10(listDict[i])) or (self.isDecrease(listDict[pre], listDict[i])):
                buyList.append(listDict[i][0])
            else:
                buyList.append(None)
            pre = i
        plt.plot(keyList, buyList, 'oy', label="buy")

        ## 매수 매도 확인
        isSell=False
        tot =0
        cell = 0
        for i in range(len(sellList)):
            if isSell == False and sellList[i] == None:
                continue
            elif isSell==False and sellList[i] != None:
                print()
                print("------- 매 수 -------")
                print("매수일 : ",keyList[i])
                print("매수금 : ",sellList[i])
                cell = sellList[i]
                cellDay = keyList[i]
                isSell=True
            elif isSell==True and buyList[i] !=None:
                print("------- 매 도 -------")
                print("매도일 : ", keyList[i])
                print("매도금 : ",buyList[i])
                print("투자일 : ",keyList[i]-cellDay)
                cell = buyList[i]-cell
                print("금번 손익 : ",cell)
                tot = tot+cell
                isSell=False
            else:
                continue

        print("TOT : ", tot)
        plt.legend()
        plt.show()



    ## 매수 조건
    # 과거값이 현재값 보다 큰면 False / 작으면 True
    def isRightUp(self,previous,current):
        if previous < current:
            return True
        else:
            return False
    # 종가가 과거값보다 현재값이 크면 True / 작으면 False
    def isRealRightUp(self,previous,current):
        return self.isRightUp(previous[0],current[0])
    # 5평선이 과거값보다 현재값 크면 True / 작으면 False
    def is5DayMeanRightUp(self,previous,current):
        return self.isRightUp(previous[1], current[1])
    # 5평선이 10평선 보다 크면 True / 작으면 False
    def isUP5to10(self,current):
        return self.isRightUp(current[2], current[1])
    # 현재값이 과거값보다 10% 증가 했으면 True / 아니면 False
    def isIncrease(self,previous,current):
        if self.isRealRightUp(previous,current):
            if current[0] -previous[0] > previous[0]*0.1:
                return True
        return False


    ## 매도 조건
    # 과거값이 현재값 보다 작으면 False / 작으면 True
    def isRightDown(self,previous,current):
        if previous > current:
            return True
        else:
            return False
    # 종가가 과거값보다 현재값이 작으면 True / 작으면 False
    def isRealRightDown(self,previous,current):
        return self.isRightDown(previous[0],current[0])
    # 5평선이 과거값보다 현재값 작으면 True / 작으면 False
    def is5DayMeanRightDown(self,previous,current):
        return self.isRightDown(previous[1], current[1])
    # 5평선이 10평선 보다 작으면 True / 작으면 False
    def isDown5to10(self,current):
        return self.isRightDown(current[2], current[1])
    # 현재값이 과거값보다 10% 하락 했으면 True / 아니면 False
    def isDecrease(self,previous,current):
        if self.isRealRightDown(previous,current):
            if previous[0] - current[0] > previous[0]*0.1:
                return True
        return False

import cx_Oracle as cx
if __name__ == "__main__":
    ## 삼성전자 6개월 전 데이터 취득 후 평균이동선 그리기
    dsn = cx.makedsn("localhost", 1521, service_name="master")
    conn = cx.connect("c##masteruser", "q7w5r1z8", dsn)

    #삼성전자
    while True:
        time.sleep(1)
        jongmok = '005930'
        test = MeanDay('005930','KS',0,6,0)

        data = test.returnAllData()
        key = sorted(data,reverse=True)
        ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클시
        if test.isRealRightUp(data[key[1]], data[key[0]]) and test.is5DayMeanRightUp(data[key[1]], data[key[0]]) and test.isUP5to10(data[key[0]]):
        ## 조건 : Real 우상향 and 5일선 우상향 and 5일선이 10일선보다 클 시 and 하루에 1000원 이상 올랐을 시
        #if (test.isRealRightUp(data[key[1]], data[key[0]]) and test.is5DayMeanRightUp(data[key[1]], data[key[0]]) and test.isUP5to10(data[key[0]])) or test.isIncrease(data[key[1]], data[key[0]]):
            try:
                print("매수")
                cur = conn.cursor()
                sql = "UPDATE stock_status SET status=1, update_tm = sysdate where jongmok ='"+jongmok+"'"
                cur.execute(sql)
                conn.commit()
                cur.close()

            except Exception as e:
                print(e)
        ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시
        #if test.isRightDown(data[key[1]], data[key[0]]) and test.is5DayMeanRightDown(data[key[1]], data[key[0]]) and test.isDown5to10(data[key[0]]):
        ## 조건 : Real 우하향 and 5일선 우하향 and 5일선이 10일선보다 작을 시 and 하루에 1000원 이상 떨어졌을 시
        if (test.isRightDown(data[key[1]], data[key[0]]) and test.is5DayMeanRightDown(data[key[1]], data[key[0]]) and test.isDown5to10(data[key[0]])) or (test.isDecrease(data[key[1]], data[key[0]])):
            try:
                print("매도")
                cur = conn.cursor()
                sql = "UPDATE stock_status SET status=2, update_tm = sysdate where jongmok ='"+jongmok+"'"
                cur.execute(sql)
                conn.commit()
                cur.close()

            except Exception as e:
                print(e)

    conn.close()

    #네이버
    #test = MeanDay('035420','KS',2,0,0)

    #카카오
    #test = MeanDay('035720','KS',1,0,0)

    # 신풍제약우
    #test = MeanDay('019175','KS',1,0,0)

    # 비씨월드제약
    #test = MeanDay('200780','KS',1,0,0)

    #test = MeanDay('000660', 'KS', 0, 7, 0)